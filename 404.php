<?php
http_response_code(404);
header("Refresh:3; index.php");
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <title>E-Banking</title>
</head>
<body>
<!--<h1 class="h1 text-center m-2 text-secondary">KerberSoki-Bank</h1>-->
<header class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
    <a class="navbar-brand col-md-3 col-lg-2 me-0 px-3" href="#">KerberSoki-Bank</a>
</header>

<div class="container-fluid mt-5 col-sm-5 border border-white rounded-3 bg-dark shadow-lg">

    <h1 class="h1 m-3 text-white text-center">404 Not Found</h1>

</div>
