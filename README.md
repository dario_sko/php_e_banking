# PHP_E_Banking

E-Banking mit PHP + Bootstrap

# Lösungsansatz

Welche Klasse werden benötigt:

- Benutzer
    - Rolle → Kunde und Angestellter
    - Vorname
    - Nachname
    - Email
    - Geburtsdatum
    - Adresse
    - PLZ
        - Kann Sich registrieren
        - Kann Sich einloggen/ausloggen
        - Kann Kontodaten ausgeben
        - Kann Tranksaktionen tätigen
- Bankkonto
    - Kontostand
    - Alle Transakationen
    - IBAN
    - BIC
    - Verfügernummer
- Transaktionen
    - Absender
    - Empfänger
    - Zahlungsreferenz
    - Verwendungszweck
    - Datum
    - Uhrzeit
    - Betrag

- Cookie?
- Session?
- Filtern?