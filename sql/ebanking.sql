-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema ebanking
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `ebanking` ;

-- -----------------------------------------------------
-- Schema ebanking
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `ebanking` DEFAULT CHARACTER SET utf8 ;
USE `ebanking` ;

-- -----------------------------------------------------
-- Table `ebanking`.`bankaccounts`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ebanking`.`bankaccounts` ;

CREATE TABLE IF NOT EXISTS `ebanking`.`bankaccounts` (
  `idbankaccounts` INT NOT NULL AUTO_INCREMENT,
  `balance` DOUBLE ZEROFILL NULL,
  `iban` VARCHAR(255) NOT NULL,
  `bic` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`idbankaccounts`),
  UNIQUE INDEX `iban_UNIQUE` (`iban` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ebanking`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ebanking`.`users` ;

CREATE TABLE IF NOT EXISTS `ebanking`.`users` (
  `iduser` INT NOT NULL AUTO_INCREMENT,
  `firstname` VARCHAR(255) NOT NULL,
  `lastname` VARCHAR(255) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `birthdate` DATE NOT NULL,
  `address` VARCHAR(255) NOT NULL,
  `postalcode` VARCHAR(45) NOT NULL,
  `username` VARCHAR(255) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `isEmployee` TINYINT NOT NULL,
  `fk_bankaccounts` INT NOT NULL,
  PRIMARY KEY (`iduser`, `fk_bankaccounts`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC) VISIBLE,
  INDEX `fk_users_bankaccounts_idx` (`fk_bankaccounts` ASC) VISIBLE,
  CONSTRAINT `fk_users_bankaccounts`
    FOREIGN KEY (`fk_bankaccounts`)
    REFERENCES `ebanking`.`bankaccounts` (`idbankaccounts`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ebanking`.`transactions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ebanking`.`transactions` ;

CREATE TABLE IF NOT EXISTS `ebanking`.`transactions` (
  `idtransactions` INT NOT NULL AUTO_INCREMENT,
  `reference` VARCHAR(255) NULL,
  `description` VARCHAR(255) NULL,
  `datetime` DATETIME NOT NULL,
  `amount` DOUBLE NOT NULL,
  `fk_sender` INT NOT NULL,
  `fk_receiver` INT NOT NULL,
  PRIMARY KEY (`idtransactions`),
  INDEX `fk_transactions_bankaccounts1_idx` (`fk_sender` ASC) VISIBLE,
  INDEX `fk_transactions_bankaccounts2_idx` (`fk_receiver` ASC) VISIBLE,
  CONSTRAINT `fk_transactions_bankaccounts1`
    FOREIGN KEY (`fk_sender`)
    REFERENCES `ebanking`.`bankaccounts` (`idbankaccounts`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_transactions_bankaccounts2`
    FOREIGN KEY (`fk_receiver`)
    REFERENCES `ebanking`.`bankaccounts` (`idbankaccounts`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `ebanking`.`bankaccounts`
-- -----------------------------------------------------
START TRANSACTION;
USE `ebanking`;
INSERT INTO `ebanking`.`bankaccounts` (`idbankaccounts`, `balance`, `iban`, `bic`) VALUES (1, 1000, 'AT697000087577784591', 'KERBERSOKI22');
INSERT INTO `ebanking`.`bankaccounts` (`idbankaccounts`, `balance`, `iban`, `bic`) VALUES (2, 20000, 'AT697000016850621736', 'KERBERSOKI22');

COMMIT;


-- -----------------------------------------------------
-- Data for table `ebanking`.`users`
-- -----------------------------------------------------
START TRANSACTION;
USE `ebanking`;
INSERT INTO `ebanking`.`users` (`iduser`, `firstname`, `lastname`, `email`, `birthdate`, `address`, `postalcode`, `username`, `password`, `isEmployee`, `fk_bankaccounts`) VALUES (1, 'Clemens', 'Kerber', 'clemens@kerber.at', '1999-06-22', 'Schnann 54', '6574', 'clemens', 'test1234', 1, 1);
INSERT INTO `ebanking`.`users` (`iduser`, `firstname`, `lastname`, `email`, `birthdate`, `address`, `postalcode`, `username`, `password`, `isEmployee`, `fk_bankaccounts`) VALUES (2, 'Dario', 'Skocibusic', 'dario@skocibusic.at', '1993-11-09', 'Eileweg 6b', '6522', 'dario', 'test1234', 0, 2);

COMMIT;


-- -----------------------------------------------------
-- Data for table `ebanking`.`transactions`
-- -----------------------------------------------------
START TRANSACTION;
USE `ebanking`;
INSERT INTO `ebanking`.`transactions` (`idtransactions`, `reference`, `description`, `datetime`, `amount`, `fk_sender`, `fk_receiver`) VALUES (1, '13411234', 'Serverkosten', '2022-01-25 12:12:00', 100, 1, 2);
INSERT INTO `ebanking`.`transactions` (`idtransactions`, `reference`, `description`, `datetime`, `amount`, `fk_sender`, `fk_receiver`) VALUES (2, '47837233', 'Gutschrift', '2022-01-20 13:22:13', 200, 2, 1);

COMMIT;

