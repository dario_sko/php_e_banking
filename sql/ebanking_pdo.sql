DROP SCHEMA IF EXISTS `ebanking` ;

CREATE SCHEMA IF NOT EXISTS `ebanking` DEFAULT CHARACTER SET utf8 ;
USE `ebanking` ;

DROP TABLE IF EXISTS `ebanking`.`bankaccounts` ;

CREATE TABLE IF NOT EXISTS `ebanking`.`bankaccounts` (
  `idbankaccounts` INT NOT NULL AUTO_INCREMENT,
  `balance` DOUBLE NULL,
  `iban` VARCHAR(255) NOT NULL,
  `bic` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`idbankaccounts`),
  UNIQUE INDEX `iban_UNIQUE` (`iban` ASC) VISIBLE)
ENGINE = InnoDB;

DROP TABLE IF EXISTS `ebanking`.`users` ;

CREATE TABLE IF NOT EXISTS `ebanking`.`users` (
  `iduser` INT NOT NULL AUTO_INCREMENT,
  `firstname` VARCHAR(255) NOT NULL,
  `lastname` VARCHAR(255) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `birthdate` DATE NOT NULL,
  `address` VARCHAR(255) NOT NULL,
  `postalcode` VARCHAR(45) NOT NULL,
  `username` VARCHAR(255) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `isEmployee` TINYINT NOT NULL,
  `fk_bankaccounts` INT NOT NULL,
  PRIMARY KEY (`iduser`, `fk_bankaccounts`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC) VISIBLE,
  INDEX `fk_users_bankaccounts_idx` (`fk_bankaccounts` ASC) VISIBLE,
  CONSTRAINT `fk_users_bankaccounts`
    FOREIGN KEY (`fk_bankaccounts`)
    REFERENCES `ebanking`.`bankaccounts` (`idbankaccounts`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

DROP TABLE IF EXISTS `ebanking`.`transactions` ;

CREATE TABLE IF NOT EXISTS `ebanking`.`transactions` (
  `idtransactions` INT NOT NULL AUTO_INCREMENT,
  `reference` VARCHAR(255) NULL,
  `description` VARCHAR(255) NULL,
  `datetime` DATETIME NOT NULL,
  `amount` DOUBLE NOT NULL,
  `fk_sender` INT NOT NULL,
  `fk_receiver` INT NOT NULL,
  PRIMARY KEY (`idtransactions`),
  INDEX `fk_transactions_bankaccounts1_idx` (`fk_sender` ASC) VISIBLE,
  INDEX `fk_transactions_bankaccounts2_idx` (`fk_receiver` ASC) VISIBLE,
  CONSTRAINT `fk_transactions_bankaccounts1`
    FOREIGN KEY (`fk_sender`)
    REFERENCES `ebanking`.`bankaccounts` (`idbankaccounts`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_transactions_bankaccounts2`
    FOREIGN KEY (`fk_receiver`)
    REFERENCES `ebanking`.`bankaccounts` (`idbankaccounts`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

START TRANSACTION;
USE `ebanking`;
INSERT INTO `ebanking`.`bankaccounts` (`idbankaccounts`, `balance`, `iban`, `bic`) VALUES (1, 100, 'AT697000087577784591', 'KERBERSOKI22');
INSERT INTO `ebanking`.`bankaccounts` (`idbankaccounts`, `balance`, `iban`, `bic`) VALUES (2, -100, 'AT697000016850621736', 'KERBERSOKI22');

COMMIT;

START TRANSACTION;
USE `ebanking`;
INSERT INTO `ebanking`.`users` (`iduser`, `firstname`, `lastname`, `email`, `birthdate`, `address`, `postalcode`, `username`, `password`, `isEmployee`, `fk_bankaccounts`) VALUES (1, 'Clemens', 'Kerber', 'clemens@kerber.at', '1999-06-22', 'Schnann 54', '6574', 'clemens', 'test1234', 1, 1);
INSERT INTO `ebanking`.`users` (`iduser`, `firstname`, `lastname`, `email`, `birthdate`, `address`, `postalcode`, `username`, `password`, `isEmployee`, `fk_bankaccounts`) VALUES (2, 'Dario', 'Skocibusic', 'dario@skocibusic.at', '1993-11-09', 'Eileweg 6b', '6522', 'dario', 'test1234', 0, 2);

COMMIT;

START TRANSACTION;
USE `ebanking`;
INSERT INTO `ebanking`.`transactions` (`idtransactions`, `reference`, `description`, `datetime`, `amount`, `fk_sender`, `fk_receiver`) VALUES (1, '13411234', 'Serverkosten', '2022-01-25 12:12:00', 100, 1, 2);
INSERT INTO `ebanking`.`transactions` (`idtransactions`, `reference`, `description`, `datetime`, `amount`, `fk_sender`, `fk_receiver`) VALUES (2, '47837233', 'Gutschrift', '2022-01-20 13:22:13', 200, 2, 1);

COMMIT;

