<?php
require_once "../models/DatabaseConnectPDO.php";

$conn = null;
$filename = "ebanking_pdo.sql";

// Verbindung zur Datenbank
try {
    $conn = new PDO("mysql:host=" . DatabaseConnectPDO::$servername, DatabaseConnectPDO::$username,
        DatabaseConnectPDO::$password);
} catch (PDOException $e) {
    echo "<script>alert('Verbindung zur Datenbank fehlgeschlagen')</script>";
    header("Refresh:3; ../index.php");
    die();
}

// Auslesen der SQL Datei
if (file_get_contents($filename)) {
    $sql = file_get_contents($filename);
} else {
    echo "<script>alert('SQL Datei nicht vorhanden!')</script>";
    header("Refresh:3; ../index.php");
    die();
}

// Ausführen der SQL Datei
$conn->exec($sql);
//$code = $conn->errorCode() == null ?? "00000";
if (strcmp($conn->errorCode(), "00000")) {
    echo "<script>alert('Ausführung der SQL Datei fehlgeschlagen')</script>";
    header("Refresh:3; ../index.php");
    die();
}

// Datenbankverbindung schließen
$conn = null;

// Weiterleitung zu index.php
header("Location: ../index.php");
