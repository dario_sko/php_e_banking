<?php
require_once "../models/User.php";
require_once "../models/Bankaccount.php";

$confirmPassword = '';
$errors = [];
$message = "";


$user = new User();

if (isset($_POST['submit'])) {
    // Erstellt für den User zuerst einen Bankaccount
    $bankaccount = new Bankaccount();
    //Speichert ID für den Foreign Key
    $bankaccountId = $bankaccount->create();


    $confirmPassword = $_POST['confirmPassword'] ?? '';

    $user->setFirstname($_POST['firstname'] ?? '');
    $user->setLastname($_POST['lastname'] ?? '');
    $user->setEmail($_POST['email'] ?? '');
    $user->setBirthdate($_POST['birthdate'] ?? '');
    $user->setAddress($_POST['address'] ?? '');
    $user->setPostalcode($_POST['postalcode'] ?? '');
    $user->setUsername($_POST['username'] ?? '');
    $user->setPassword($_POST['password'] ?? '');
    $user->setFkbankaccounts($bankaccountId);

    if ($user->save()) {
        $message = "<div class='container row col-12 line mb-3 m-auto justify-content-center alert alert-success'
                    style='border:1px solid #cecece;'><ul><li>Registrierung erfolgreich! Sie werden auf die Login-Seite
                     geleitet!</li></ul></div>";
        header("Refresh:3; url=../index.php");
    } else {
        $message = "<div class='container row col-12 line mb-3 m-auto justify-content-center alert alert-danger' 
                    style='border:1px solid #cecece;'><ul><li>Registrierung fehlgeschlagen!</li></ul></div>";
        header("Refresh:2; url=../index.php");

    }
}

?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <title>Registrieren</title>
    <script type="text/javascript" src="js/index.js"></script>

</head>
<body>
<header class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
    <a class="navbar-brand col-md-3 col-lg-2 me-0 px-3" href="#">KerberSoki-Bank</a>
</header>

<?php
if (!empty($message)) {
    echo $message;
    exit();
}
if (!empty($user->getErrors())) {
    $errors[] = $user->getErrors();

    if (!empty($errors)) {
        echo "<div class='container row col-12 line mb-3 m-auto justify-content-center alert alert-danger'  style='border:1px solid #cecece;'><ul>";
        foreach ($errors as $key) {
            foreach ($key as $message)
                echo "<li>" . $message . "</li>";
        }
        echo "</ul></div>";
    }
}
?>
<form action="registrierung.php" method="post">
    <div class="container border row col-12 mt-5 m-auto border-white rounded-3 bg-dark text-white">
        <!--    <div class="container row col-12 line m-auto justify-content-center" style="border:1px solid #cecece;">-->
        <h1 class="m">Registrieren</h1>

        <form class="row g-3">
            <!--Vorname-->
            <div class="col-sm-6">
                <label for="inputFirstName" class="mt-3 form-label">Vorname*</label>
                <input type="text"
                       name="firstname"
                       class="form-control <?= $user->hasErrors('firstName') ? 'is-invalid' : '' ?>"
                       value="<?= htmlspecialchars($user->getFirstname()) ?>"
                       id="inputFirstName"
                       maxlength="20"
                       required
                />
            </div>
            <!--Nachname-->
            <div class="col-sm-6">
                <label for="inputLastName" class="mt-3 form-label">Nachname*</label>
                <input
                        type="text"
                        name="lastname"
                        class="form-control <?= $user->hasErrors('lastName') ? 'is-invalid' : '' ?>"
                        value="<?= htmlspecialchars($user->getLastname()) ?>"
                        id="inputLastName"
                        maxlength="20"
                        required
                />
            </div>
            <!--Email-->
            <div class="col-sm-6">
                <label for="inputEmail" class="mt-3 form-label">E-Mail*</label>
                <input
                        type="email"
                        name="email"
                        class="form-control <?= $user->hasErrors('email') ? 'is-invalid' : '' ?>"
                        value="<?= htmlspecialchars($user->getEmail()) ?>"
                        id="inputEmail"
                        required
                />
            </div>
            <!--Geburtsdatum-->
            <div class="col-sm-6">
                <label for="inputBirthday" class="mt-3 form-label">Geburtstag*</label>
                <input
                        type="date"
                        name="birthdate"
                        class="form-control <?= $user->hasErrors('birthdate') ? 'is-invalid' : '' ?>"
                        value="<?= htmlspecialchars($user->getBirthdate()) ?>"
                        id="inputBirthday"
                        required
                />
            </div>
            <!--Adresse-->
            <div class="col-sm-6">
                <label for="inputAddress" class="mt-3 form-label">Adresse*</label>
                <input
                        type="text"
                        name="address"
                        class="form-control <?= $user->hasErrors('address') ? 'is-invalid' : '' ?>"
                        value="<?= htmlspecialchars($user->getAddress()) ?>"
                        id="inputAddresse"
                        maxlength="20"
                        required
                />
            </div>
            <!--Postleitzahl-->
            <div class="col-sm-6">
                <label for="inputPostalCode" class="mt-3 form-label">Postleitzahl*</label>
                <input
                        type="text"
                        name="postalcode"
                        class="form-control <?= $user->hasErrors('postalcode') ? 'is-invalid' : '' ?>"
                        value="<?= htmlspecialchars($user->getPostalcode()) ?>"
                        id="inputPostalCode"
                        maxlength="8"
                        required
                />
            </div>
            <!--Benutzername-->
            <div class="col-sm-6 mt-3">
                <label for="inputUserName" class="mt-3 form-label">Benutzername*</label>
                <input type="text"
                       name="username"
                       class="form-control <?= $user->hasErrors('username') ? 'is-invalid' : '' ?>"
                       value="<?= htmlspecialchars($user->getUsername()) ?>"
                       required
                       maxlength="10"
                       id="inputUserName">
            </div>
            <!--Passwort-->
            <div class="col-sm-6 mt-3">
                <label for="inputPassword" class="mt-3 form-label">Passwort*</label>
                <input type="password"
                       name="password"
                       class="form-control <?= $user->hasErrors('password') || $user->hasErrors('passwordCheck') ? 'is-invalid' : '' ?>"
                       value="<?= htmlspecialchars("") ?>"
                       required
                       minlength="6"
                       id="inputPassword">
            </div>
            <!--Passwort bestätigen-->
            <div class="col-sm-6 offset-6">
                <label for="inputPasswordConfirm" class="mt-3 form-label">Passwort Bestätigen*</label>
                <input type="password"
                       name="confirmPassword"
                       class="form-control <?= $user->hasErrors('passwordCheck') ? 'is-invalid' : '' ?>"
                       value="<?= htmlspecialchars("") ?>"
                       required
                       minlength="6"
                       id="inputPasswordConfirm">
            </div>
            <!--Button-->
            <div class="col-12 mb-3">
                <input type="submit" name="submit" value="Registrieren" class="btn btn-primary"></input>
                <a href="../index.php" class="btn btn-secondary ms-3">Zurück</a>
            </div>
        </form>
</body>
</html>