<?php

class DatabaseConnectPDO
{
    private static $conn;

//    public static $servername = "192.168.230.138";
    public static $servername = "192.168.227.21";

    public static $username = "root";
    public static $password = "123";
    public static $dbname = "ebanking";

    private function __construct()
    {
    }

    public static function connect()
    {
        if (self::$conn == null) {
            try {
                self::$conn = new PDO("mysql:host=" . self::$servername . ";dbname=" . self::$dbname, self::$username, self::$password);

                self::$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
//                echo "Connected successfully";
                return self::$conn;
            } catch (PDOException $exception) {
                echo "Connection failed: " . $exception->getMessage();
            }
        } else {
            return self::$conn;
        }
    }

}