<?php
require_once "DatabaseConnectPDO.php";
require_once "DatabaseCRUD.php";


class Transaction implements DatabaseCRUD
{
    private $idtransactions;
    private $reference;
    private $description;
    private $datetime;
    private $amount;
    private $fk_sender;
    private $fk_receiver;

    private $error = [];

    public function getReceiverIbanByTransaction()
    {
        $conn = DatabaseConnectPDO::connect();
        $query = 'SELECT bankaccounts.iban
                    FROM bankaccounts
                    JOIN transactions ON transactions.fk_receiver = bankaccounts.idbankaccounts
                    WHERE transactions.fk_receiver = ?';
        $stmt = $conn->prepare($query);
        $stmt->execute(array($this->fk_receiver));
        $item = $stmt->fetch();
        $conn = null;
        return $item['iban'] ?? null;
    }

    public function getSenderIbanByTransaction()
    {
        $conn = DatabaseConnectPDO::connect();
        $query = 'SELECT bankaccounts.iban
                    FROM bankaccounts
                    JOIN transactions ON transactions.fk_sender = bankaccounts.idbankaccounts
                    WHERE transactions.fk_sender = ?';
        $stmt = $conn->prepare($query);
        $stmt->execute(array($this->fk_sender));
        $item = $stmt->fetch();
        $conn = null;
        return $item['iban'] ?? null;
    }

    public static function getAllFilteredbyBankaccount($idbankaccount, $textsearch, $amountfrom, $amountto,
                                                       $datefrom, $dateto)
    {
        $conn = DatabaseConnectPDO::connect();
        if (!empty($textsearch)) {
            $query = "SELECT idtransactions, reference, description, datetime, amount, fk_sender, fk_receiver
                    FROM transactions
                    JOIN bankaccounts as sender ON transactions.fk_sender = sender.idbankaccounts
                    JOIN bankaccounts as receiver ON transactions.fk_receiver = receiver.idbankaccounts
                    WHERE (transactions.fk_sender = :idbankaccount or transactions.fk_receiver = :idbankaccount) and transactions.description LIKE CONCAT('%', :textsearch, '%')
                    ORDER BY idtransactions";
            $stmt = $conn->prepare($query);
            $stmt->bindParam(":idbankaccount", $idbankaccount);
            $stmt->bindParam(":textsearch", $textsearch);
        } else if (!empty($amountfrom) && !empty($amountto)) {
            $query = "SELECT idtransactions, reference, description, datetime, amount, fk_sender, fk_receiver
                    FROM transactions
                    JOIN bankaccounts as sender ON transactions.fk_sender = sender.idbankaccounts
                    JOIN bankaccounts as receiver ON transactions.fk_receiver = receiver.idbankaccounts
                    WHERE (transactions.fk_sender = :idbankaccount or transactions.fk_receiver = :idbankaccount) and (transactions.amount BETWEEN :amountfrom AND :amountto)
                    ORDER BY idtransactions";
            $stmt = $conn->prepare($query);
            $stmt->bindParam(":idbankaccount", $idbankaccount);
            $stmt->bindParam(":amountfrom", $amountfrom);
            $stmt->bindParam(":amountto", $amountto);
        } elseif (!empty($datefrom) && !empty($dateto)) {
            $query = "SELECT idtransactions, reference, description, datetime, amount, fk_sender, fk_receiver
                    FROM transactions
                    JOIN bankaccounts as sender ON transactions.fk_sender = sender.idbankaccounts
                    JOIN bankaccounts as receiver ON transactions.fk_receiver = receiver.idbankaccounts
                    WHERE (transactions.fk_sender = :idbankaccount or transactions.fk_receiver = :idbankaccount) and (date(transactions.datetime) BETWEEN :datefrom AND :dateto)
                    ORDER BY idtransactions";
            $stmt = $conn->prepare($query);
            $stmt->bindParam(":idbankaccount", $idbankaccount);
            $stmt->bindParam(":datefrom", $datefrom);
            $stmt->bindParam(":dateto", $dateto);
        } else {
            return Transaction::getAllByBankacoount($idbankaccount);
        }

        $stmt->execute();
        $items = $stmt->fetchAll(PDO::FETCH_CLASS, "Transaction");
        $conn = null;
        return $items ?? null;

    }

    public static function getAllByBankacoount($idbankaccount)
    {
        $conn = DatabaseConnectPDO::connect();
        $query = 'SELECT idtransactions, reference, description, datetime, amount, fk_sender, fk_receiver
                    FROM transactions
                    JOIN bankaccounts as sender ON transactions.fk_sender = sender.idbankaccounts
                    JOIN bankaccounts as receiver ON transactions.fk_receiver = receiver.idbankaccounts
                    WHERE transactions.fk_sender = ? OR transactions.fk_receiver = ? ORDER BY idtransactions';
        $stmt = $conn->prepare($query);
        $stmt->execute(array($idbankaccount, $idbankaccount));
        $items = $stmt->fetchAll(PDO::FETCH_CLASS, 'Transaction');
        $conn = null;
        return $items ?? null;
    }

    public function create()
    {
        try {
            $conn = DatabaseConnectPDO::connect();
            $query = "INSERT INTO `ebanking`.`transactions`
                    (`reference`,
                    `description`,
                    `datetime`,
                    `amount`,
                    `fk_sender`,
                    `fk_receiver`)
                    VALUES
                    (:reference,
                    :description,
                    :datetime,
                    :amount,
                    :fk_sender,
                    :fk_receiver)";
            $stmt = $conn->prepare($query);
            $stmt->bindParam(":reference", $this->reference);
            $stmt->bindParam(":description", $this->description);
            $stmt->bindParam(":datetime", $this->datetime);
            $stmt->bindParam(":amount", $this->amount);
            $stmt->bindParam(":fk_sender", $this->fk_sender);
            $stmt->bindParam(":fk_receiver", $this->fk_receiver);
            $item = $stmt->execute();
            $conn = null;
            return $item;
        } catch (PDOException $e) {
        }
    }

    public static function get($id)
    {
        $conn = DatabaseConnectPDO::connect();
        $query = 'SELECT * FROM transactions WHERE idtransactions = ?';
        $stmt = $conn->prepare($query);
        $stmt->execute(array($id));
        $item = $stmt->fetchObject('Transaction');
        $conn = null;
        return $item ?? null;
    }

    public static function getAll()
    {
        $conn = DatabaseConnectPDO::connect();
        $query = 'SELECT * FROM transactions';
        $stmt = $conn->prepare($query);
        $stmt->execute();
        $items = $stmt->fetchAll(PDO::FETCH_CLASS, 'Transaction');
        $conn = null;
        return $items ?? null;
    }

    public function update()
    {
        $conn = DatabaseConnectPDO::connect();
        $query = 'UPDATE bankaccounts SET reference=?, description=?, datetime=?, amount=?, fk_sender=?, fk_receiver=?';
        $stmt = $conn->prepare($query);
        $stmt->execute(array($this->reference, $this->description, $this->datetime, $this->amount, $this->fk_sender, $this->fk_receiver));
        $conn = null;
    }

    public static function delete($id)
    {
        $conn = DatabaseConnectPDO::connect();
        $query = "DELETE FROM `ebanking`.`transactions`
                    WHERE idtransactions=:idtransactions";
        $stmt = $conn->prepare($query);
        $stmt->bindParam(":idtransactions", $id);
        $stmt->execute();
        $conn = null;
    }

    public function getDateAndTimeFormat()
    {
        return date_format(date_create($this->datetime), "d.m.Y H:i:s");
    }

    public function validateTransaction($receiver, $amount, $description, $reference)
    {
        return $this->validateReceiverTransaction($receiver) & $this->validateAmountTransaction($amount) & $this->validateDescriptionTransaction($description)
            & $this->validateReferenceTransaction($reference);
    }

    public function validateReceiverTransaction($receiver)
    {
        if (strlen($receiver) == 0) {
            $this->error['receiver'] = "IBAN darf nicht leer sein.";
            return false;
        } elseif (strlen($receiver) > 20) {
            $this->error['receiver'] = "IBAN ist zu lang.";
            return false;
        } else {
            return true;
        }
    }

    public function validateAmountTransaction($amount)
    {

        if (!is_numeric($amount)) {
            $this->error['amount'] = "Nur Zahlen als Betrag erlaubt.";
            return false;
        } elseif ($amount <= 0) {
            $this->error['amount'] = "Der Betrag darf nicht 0 oder negativ sein.";
            return false;
        } else {
            return true;
        }
    }

    public function validateDescriptionTransaction($description)
    {
        if (strlen($description) == 0) {
            $this->error['$description'] = "Verwendungszweck darf nicht leer sein";
            return false;
        } else if (strlen($description) > 30) {
            $this->error['$description'] = "Verwendungszweck zu lang";
            return false;
        } else {
            return true;
        }
    }

    public function validateReferenceTransaction($reference)
    {
        if (strlen($reference) == 0) {
            $this->error['reference'] = "Zahlungsreferenz darf nicht leer sein";
            return false;
        } else if (strlen($reference) > 10) {
            $this->error['reference'] = "Zahlungsreferenz zu lang";
            return false;
        } else {
            return true;
        }
    }

    /**
     * @return mixed
     */
    public function getIdtransactions()
    {
        return $this->idtransactions;
    }

    /**
     * @param mixed $idtransactions
     */
    public function setIdtransactions($idtransactions): void
    {
        $this->idtransactions = $idtransactions;
    }

    /**
     * @return mixed
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param mixed $reference
     */
    public function setReference($reference): void
    {
        $this->reference = $reference;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * @param mixed $datetime
     */
    public function setDatetime($datetime): void
    {
        $this->datetime = $datetime;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getFkSender()
    {
        return $this->fk_sender;
    }

    /**
     * @param mixed $fk_sender
     */
    public function setFkSender($fk_sender): void
    {
        $this->fk_sender = $fk_sender;
    }

    /**
     * @return mixed
     */
    public function getFkReceiver()
    {
        return $this->fk_receiver;
    }

    /**
     * @param mixed $fk_receiver
     */
    public function setFkReceiver($fk_receiver): void
    {
        $this->fk_receiver = $fk_receiver;
    }

    public function hasError($field)
    {
        return isset($this->error[$field]);
    }

    public function getError()
    {
        return $this->error;
    }


}