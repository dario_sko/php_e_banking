<?php
require_once "DatabaseConnectPDO.php";
require_once "DatabaseCRUD.php";

class User implements DatabaseCRUD
{
    private $iduser = 0;
    private $isEmployee = 0;
    private $firstname;
    private $lastname;
    private $email;
    private $birthdate;
    private $address;
    private $postalcode;
    private $username;
    private $password;
    private $fk_bankaccounts;

    private $errors = [];

    public function login()
    {
        if (isset($_POST['submit'])) {
            $this->username = $_POST['username'];
            $this->password = $_POST['password'];
            $allUsers = self::getAll();
            foreach ($allUsers as $user) {
                if ($this->username == $user->getUsername() && $this->password == $user->getPassword()) {
                    $this->loadUser($this->username);
                    $s = serialize($this); // Objektumwandlung in serialisierte Zeichenkette
                    $_SESSION['user'] = $s; // Speicherung der serialisierten Zeichenkette in Session Array
                    return true;
                }
            }
            $this->errors['login'] = "Logindaten sind falsch!";
            return false;
        }
    }

    public static function isLoggedIn()
    {
        if (isset($_SESSION['user'])) {
            return true;
        } else {
            return false;
        }
    }

    public static function logout()
    {
        unset($_SESSION['user']);
    }

    public function save()
    {
        if ($this->validate()) {
            if ($this->iduser != null && $this->iduser > 0) {
                $this->update();
            } else {
                $this->iduser = $this->create();
                if ($this->iduser == false) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public function create()
    {
        try {
            $conn = DatabaseConnectPDO::connect();
            $query = 'INSERT INTO `ebanking`.`users`(`firstname`,`lastname`,`email`,`birthdate`,`address`,`postalcode`,`username`,`password`,`isEmployee`,`fk_bankaccounts`)
                    VALUES
                    (:firstname,
                    :lastname,
                    :email,
                    :birthdate,
                    :address,
                    :postalcode,
                    :username,
                    :password,
                    :isEmployee,
                    :fk_bankaccounts)';
            $stmt = $conn->prepare($query);
            $stmt->bindParam(':firstname', $this->firstname);
            $stmt->bindParam(':lastname', $this->lastname);
            $stmt->bindParam(':email', $this->email);
            $stmt->bindParam(':birthdate', $this->birthdate);
            $stmt->bindParam(':address', $this->address);
            $stmt->bindParam(':postalcode', $this->postalcode);
            $stmt->bindParam(':username', $this->username);
            $stmt->bindParam(':password', $this->password);
            $stmt->bindParam(':isEmployee', $this->isEmployee);
            $stmt->bindParam(':fk_bankaccounts', $this->fk_bankaccounts);

            $stmt->execute();

            $id = $conn->lastInsertId();
            $conn = null;
            return $id;
        } catch (PDOException $e) {
            // Löscht Bankaccount falls User-Erstellung fehlschlägt
            Bankaccount::delete($this->fk_bankaccounts);
            echo "Error: " . $e->getMessage();
            return false;
        }
    }

    public static function get($id)
    {
        $conn = DatabaseConnectPDO::connect();
        $query = 'SELECT * FROM users WHERE iduser = ?';
        $stmt = $conn->prepare($query);
        $stmt->execute(array($id));
        $item = $stmt->fetchObject('User');
        $conn = null;
        return $item ?? null;
    }

    public static function getAll()
    {
        $conn = DatabaseConnectPDO::connect();
        $query = 'SELECT * FROM users';
        $stmt = $conn->prepare($query);
        $stmt->execute();
        $items = $stmt->fetchAll(PDO::FETCH_CLASS, 'User');
        $conn = null;
        return $items ?? null;
    }

    public function update()
    {
        $conn = DatabaseConnectPDO::connect();
        $query = 'UPDATE users SET firstname = ?, lastname = ?, email = ?, birthdate = ?, address = ?,
                  postalcode = ?, username = ?, password = ?, isEmployee = ?';
        $stmt = $conn->prepare($query);
        $stmt->execute(array($this->firstname, $this->lastname, $this->email, $this->birthdate, $this->address,
            $this->postalcode, $this->username, $this->password, $this->isEmployee));
        $conn = null;
    }

    public static function delete($id)
    {
        $conn = DatabaseConnectPDO::connect();
        $query = 'DELETE FROM users WHERE iduser = ?';
        $stmt = $conn->prepare($query);
        $stmt->execute(array($id));
        $conn = null;
    }

    public function loadUser($username)
    {
        $conn = DatabaseConnectPDO::connect();
        $query = 'SELECT * FROM users WHERE username = ?';
        $stmt = $conn->prepare($query);
        $stmt->execute(array($username));
        $item = $stmt->fetchObject("User");
        $conn = null;

        $this->setIduser($item->getIduser());
        $this->setFirstname($item->getFirstname());
        $this->setLastname($item->getLastname());
        $this->setBirthdate($item->getBirthdate());
        $this->setAddress($item->getAddress());
        $this->setPostalcode($item->getPostalcode());
        $this->setEmail($item->getEmail());
        $this->setIsEmployee($item->getIsEmployee());
        $this->setFkbankaccounts($item->getFkbankaccounts());
    }

    public function validate()
    {
        return $this->validateFirstname() & $this->validateLastname() & $this->validateEmail() & $this->validateBirthdate() & $this->validateAddress() &
            $this->validatePostalcode() & $this->validateUsername() & $this->validatePassword() & $this->validateIsEmployee() & $this->checkPassword()
            & $this->checkUsername();
    }

    public function checkUsername()
    {
        $conn = DatabaseConnectPDO::connect();
        $query = 'SELECT * FROM users WHERE username=:username';
        $stmt = $conn->prepare($query);
        $stmt->bindParam(':username', $this->username);
        $stmt->execute();

        $item = $stmt->fetch();

        if ($item == false) {
            return true;
        } else {
            $this->errors['usernameCheck'] = "Benutzername schon vorhanden!";
            return false;
        }
    }

    public function checkPassword()
    {
        if ($this->password === $_POST["confirmPassword"]) {
            return true;
        } else {
            $this->errors['passwordCheck'] = "Passwörter stimmen nicht überein!";
            return false;
        }
    }

    public function validateFirstname()
    {
        if (strlen($this->firstname) == 0) {
            $this->errors['firstName'] = "Vorname darf nicht leer sein";
            return false;
        } else if (strlen($this->firstname) > 20) {
            $this->errors['firstName'] = "Vorname zu lang";
            return false;
        } else {
            return true;
        }
    }

    public function validateLastname()
    {
        if (strlen($this->lastname) == 0) {
            $this->errors['lastName'] = "Nachname darf nicht leer sein";
            return false;
        } else if (strlen($this->lastname) > 20) {
            $this->errors['lastName'] = "Nachname zu lang";
            return false;
        } else {
            return true;
        }
    }

    public function validateEmail()
    {
        if (strlen($this->email) == 0 && !filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            $this->errors['email'] = "E-Mail ungültig";
            return false;
        } else {
            return true;
        }
    }

    public function validateBirthdate()
    {
        try {
            if ($this->birthdate == "") {
                $this->errors['birthdate'] = "Geburtsdatum darf nicht leer sein";
                return false;
            } else if (new DateTime($this->birthdate) > new DateTime()) {
                $this->errors['birthdate'] = "Geburtsdatum darf nicht in der Zukunft liegen";
                return false;
            } else {
                return true;
            }
        } catch (Exception $e) {
            $this->errors['birthdate'] = "Geburtsdatum ungültig";
            return false;
        }
    }

    public function validateAddress()
    {
        if (strlen($this->address) == 0) {
            $this->errors['address'] = "Adresse darf nicht leer sein";
            return false;
        } else if (strlen($this->address) > 20) {
            $this->errors['address'] = "Adresse zu lang";
            return false;
        } else {
            return true;
        }
    }

    public function validatePostalcode()
    {
        if (strlen($this->postalcode) == 0) {
            $this->errors['postalcode'] = "Postleitzahl darf nicht leer sein";
            return false;
        } else if (strlen($this->postalcode) > 8) {
            $this->errors['postalcode'] = "Postleitzahl zu lang";
            return false;
        } else {
            return true;
        }
    }

    public function validateUsername()
    {
        if (strlen($this->username) == 0) {
            $this->errors['username'] = "Benutzername darf nicht leer sein";
            return false;
        } else if (strlen($this->username) > 10) {
            $this->errors['username'] = "Benutzername zu lang";
            return false;
        } else {
            return true;
        }
    }

    public function validatePassword()
    {
        if (strlen($this->password) == 0) {
            $this->errors['password'] = "Passwort darf nicht leer sein";
            return false;
        } else if (strlen($this->password) <= 6) {
            $this->errors['password'] = "Passwort muss mindestens 6 Zeichen enthalten.";
            return false;
        } else {
            return true;
        }
    }

    public function validateIsEmployee()
    {

        if ($this->isEmployee == 0) {
            return true;
        } elseif ($this->isEmployee == 1) {
            return true;
        } else {
            $this->errors['role'] = "Der TINYINT darf nur 0 für false und 1 für true enthalten.";
        }
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function hasErrors($field)
    {
        return isset($this->errors[$field]);
    }

    /**
     * @return int
     */
    public function getIduser(): int
    {
        return $this->iduser;
    }

    /**
     * @param int $iduser
     */
    public function setIduser(int $iduser): void
    {
        $this->iduser = $iduser;
    }

    /**
     * @return mixed
     */
    public function getIsEmployee()
    {
        return $this->isEmployee;
    }

    /**
     * @param mixed $isEmployee
     */
    public function setIsEmployee($isEmployee): void
    {
        $this->isEmployee = $isEmployee;
    }

    /**
     * @return mixed
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param mixed $firstname
     */
    public function setFirstname($firstname): void
    {
        $this->firstname = $firstname;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param mixed $lastname
     */
    public function setLastname($lastname): void
    {
        $this->lastname = $lastname;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * @param mixed $birthdate
     */
    public function setBirthdate($birthdate): void
    {
        $this->birthdate = $birthdate;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address): void
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getPostalcode()
    {
        return $this->postalcode;
    }

    /**
     * @param mixed $postalcode
     */
    public function setPostalcode($postalcode): void
    {
        $this->postalcode = $postalcode;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username): void
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password): void
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getFkbankaccounts()
    {
        return $this->fk_bankaccounts;
    }

    /**
     * @param mixed $fk_bankaccounts
     */
    public function setFkbankaccounts($fk_bankaccounts): void
    {
        $this->fk_bankaccounts = $fk_bankaccounts;
    }
}