<?php
require_once "DatabaseConnectPDO.php";
require_once "DatabaseCRUD.php";

class Bankaccount implements DatabaseCRUD
{
    private $idbankaccounts;
    private $balance = 0.0;
    private $iban;
    private $bic = "KERBERSOKI22";

    public function generateIban()
    {
        $iban = "AT6970000";
        for ($i = 0; $i < 11; $i++) {
            $iban .= random_int(0, 9);
        }
        if ($this->checkIban($iban)) {
            return $iban;
        }
    }

    public function checkIban($iban)
    {
        $conn = DatabaseConnectPDO::connect();
        $query = 'SELECT * FROM bankaccounts WHERE iban=:iban';
        $stmt = $conn->prepare($query);
        $stmt->bindParam(':iban', $iban);
        $stmt->execute();

        $item = $stmt->fetch();

        if ($item == false) {
            return true;
        } else {
            $this->generateIban();
        }
    }

    public function create()
    {
        $this->iban = $this->generateIban();
        try {
            $conn = DatabaseConnectPDO::connect();
            $query = 'INSERT INTO `ebanking`.`bankaccounts`(`balance`,`iban`,`bic`)
                VALUES
                (:balance,
                :iban,
                :bic)';
            $stmt = $conn->prepare($query);
            $stmt->bindParam(':balance', $this->balance);
            $stmt->bindParam(':iban', $this->iban);
            $stmt->bindParam(':bic', $this->bic);
            $stmt->execute();

            // Gibt die ID des zuletzt eingefügten Statements
            $id = $conn->lastInsertId();

            $conn = null;
            return $id;
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
    }


    public static function updateAfterTransaction($amount, $idsender, $idreceiver)
    {
        $sender = self::get($idsender);
        $receiver = self::get($idreceiver);

        $senderNewBalance = $sender->getBalance();
        $receiverNewBalance = $receiver->getBalance();

        if ($idsender != $idreceiver) {
            $senderNewBalance = $sender->getBalance() - $amount;
            $receiverNewBalance = $receiver->getBalance() + $amount;
        }

        $conn = DatabaseConnectPDO::connect();
        $query = 'UPDATE bankaccounts SET balance = ? WHERE idbankaccounts = ?;
                  UPDATE bankaccounts SET balance = ? WHERE idbankaccounts = ?';
        $stmt = $conn->prepare($query);
        $stmt->execute(array($senderNewBalance, $sender->getIdbankaccounts(), $receiverNewBalance, $receiver->getIdbankaccounts()));
        $conn = null;
    }

    public function update()
    {
        $conn = DatabaseConnectPDO::connect();
        $query = 'UPDATE bankaccounts SET balance = ?';
        $stmt = $conn->prepare($query);
        $stmt->execute(array($this->balance));
        $conn = null;
    }

    public static function getIdByIban($iban)
    {
        $conn = DatabaseConnectPDO::connect();
        $query = 'SELECT idbankaccounts FROM bankaccounts WHERE iban = ?';
        $stmt = $conn->prepare($query);
        $stmt->execute(array($iban));
        $item = $stmt->fetch();
        $conn = null;
        return $item ?? null;
    }

    public static function get($id)
    {
        $conn = DatabaseConnectPDO::connect();
        $query = 'SELECT * FROM bankaccounts WHERE idbankaccounts = ?';
        $stmt = $conn->prepare($query);
        $stmt->execute(array($id));
        $item = $stmt->fetchObject('Bankaccount');
        $conn = null;
        return $item ?? null;
    }

    public static function getAll()
    {
        $conn = DatabaseConnectPDO::connect();
        $query = 'SELECT * FROM bankaccounts';
        $stmt = $conn->prepare($query);
        $stmt->execute();
        $items = $stmt->fetchAll(PDO::FETCH_CLASS, 'Bankaccount');
        $conn = null;
        return $items ?? null;
    }


    public static function delete($id)
    {
        $conn = DatabaseConnectPDO::connect();
        $query = "DELETE FROM `ebanking`.`bankaccounts`
                    WHERE idbankaccounts=:idbankaccounts";
        $stmt = $conn->prepare($query);
        $stmt->bindParam(":idbankaccounts", $id);
        $stmt->execute();
        $conn = null;
    }

    /**
     * @return mixed
     */
    public function getIdbankaccounts()
    {
        return $this->idbankaccounts;
    }

    /**
     * @param mixed $idbankaccounts
     */
    public function setIdbankaccounts($idbankaccounts): void
    {
        $this->idbankaccounts = $idbankaccounts;
    }

    /**
     * @return float
     */
    public function getBalance(): float
    {
        return $this->balance;
    }

    /**
     * @param float $balance
     */
    public function setBalance(float $balance): void
    {
        $this->balance = $balance;
    }

    /**
     * @return array
     */
    public function getTransactions(): array
    {
        return $this->transactions;
    }

    /**
     * @param array $transactions
     */
    public function setTransactions(array $transactions): void
    {
        $this->transactions = $transactions;
    }

    /**
     * @return string|void
     */
    public function getIban(): string
    {
        return $this->iban;
    }

    /**
     * @param string|void $iban
     */
    public function setIban(string $iban): void
    {
        $this->iban = $iban;
    }

    /**
     * @return string
     */
    public function getBic(): string
    {
        return $this->bic;
    }

    /**
     * @param string $bic
     */
    public function setBic(string $bic): void
    {
        $this->bic = $bic;
    }


}