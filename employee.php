<?php
session_start();
ob_start();
require_once "models/Transaction.php";
require_once "models/User.php";

if (isset($_GET['logout'])) {
    User::logout();
    header("Location: index.php");
    die();
}

if (!User::isLoggedIn()) {
    header("Location: 404.php");
    die();
}

?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <title>E-Banking</title>
</head>
<body>

<header class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow-lg">
    <a class="navbar-brand col-md-3 col-lg-2 me-0 px-3" href="#">KerberSoki-Bank</a>
    <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse"
            data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false"
            aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="navbar-nav">
        <form method="post" action="dashboard.php">
            <div class="nav-item text-nowrap">
                <a class="nav-link px-3" href="?logout=true">Logout</a>
            </div>
        </form>
    </div>
</header>

<!--  Linke Seite -->
<div class="container-fluid">
    <div class="row">
        <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block">
            <div class="position-sticky pt-3">

                <h5 class="px-3 fs-4">
                    Kundenprofil
                </h5>
                <!--User Profil Ausgabe-->
                <?php
                $user = unserialize($_SESSION['user']);
                ?>

                <ul class="nav flex-column px-3">
                    <li>
                        <p class="my-0 fw-bold">ID:<br></p>
                        <p class="my-0"><?= $user->getIduser() ?></p>
                    </li>
                    <li class="my-0">
                        <p class="my-0 fw-bold">Vorname:<br></p>
                        <p class="my-0"><?= $user->getFirstname() ?></p>
                    </li>
                    <li class="my-0">
                        <p class="my-0 fw-bold">Nachname:<br></p>
                        <p class="my-0"><?= $user->getLastname() ?></p>
                    </li>
                    <li class="my-0">
                        <p class="my-0 fw-bold">Geburtstag:<br></p>
                        <p class="my-0"><?= $user->getBirthdate() ?></p>
                    </li>
                    <li class="my-0">
                        <p class="my-0 fw-bold">E-Mail:<br></p>
                        <p class="my-0"><?= $user->getEmail() ?></p>
                    </li>
                    <li class="my-0">
                        <p class="my-0 fw-bold">Adresse:<br></p>
                        <p class="my-0"><?= $user->getAddress() ?></p>
                    </li>
                    <li class="my-0">
                        <p class="my-0 fw-bold">PLZ:<br></p>
                        <p class="my-0"><?= $user->getPostalcode() ?></p>
                    </li>
                </ul>

                <h5 class="px-3 fs-4 mt-3">
                    Bankkonto
                </h5>
                <!--Bankkonto Ausgabe-->
                <?php
                require_once "models/Bankaccount.php";
                $bankaccount = Bankaccount::get($user->getFkbankaccounts());

                ?>
                <ul class="nav flex-column mb-2 px-3">
                    <li class="my-0">
                        <p class="my-0 fw-bold">Kontostand:<br></p>
                        <p class="my-0"><?= number_format($bankaccount->getBalance(), 2) ?> €</p>
                    </li>
                    <li class="my-0">
                        <p class="my-0 fw-bold">IBAN:<br></p>
                        <p class="my-0"><?= $bankaccount->getIban() ?></p>
                    </li>
                    <li class="my-0">
                        <p class="my-0 fw-bold">BIC:<br></p>
                        <p class="my-0"><?= $bankaccount->getBic() ?></p>
                    </li>
                </ul>
            </div>
        </nav>

        <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4 mt-3">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Mitarbeiter</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                </div>
            </div>

            <?php

            $bankaccounts = Bankaccount::getAll();
            $receiver = '';
            $amount = '';
            $description = '';
            $reference = '';


            if (isset($_POST['submit'])) {

                $receiver = isset($_POST['receiver']) ? $_POST['receiver'] : '';
                $amount = isset($_POST['amount']) ? $_POST['amount'] : '';
                $description = isset($_POST['bar']) ? $_POST['bar'] : '';
                $reference = isset($_POST['reference']) ? $_POST['reference'] : '';

                $transaction = new Transaction();


                if ($transaction->validateTransaction($receiver, $amount, $description, $reference)) {
                    echo "<p class='alert alert-success'>Die eingegebenen Daten sind in Ordnung!</p>";
                } else {
                    echo "<div class='alert alert-danger'><p>Die eingegebenen Daten sind fehlerhaft!</p><ul>";
                    // $error[] = $transaction->getError();
                    foreach ($transaction->getError() as $key => $value) {
                        echo "<li>" . $value . "</li>";
                    }
                    echo "</ul></div>";
                    header("Refresh:2; url=employee.php");
                    die();
                }

                date_default_timezone_set("Europe/Vienna");


                if ($description == 0) {
                    $description = "Barauszahlung";
                    $idsender = Bankaccount::getIdByIban($receiver)['idbankaccounts'];
                    $idreceiver = $bankaccount->getIdbankaccounts();
                } else {
                    $description = "Bareinzahlung";
                    $idsender = $bankaccount->getIdbankaccounts();
                    $idreceiver = Bankaccount::getIdByIban($receiver)['idbankaccounts'];
                }

                $transaction->setFkSender($idsender);
                $transaction->setFkReceiver($idreceiver);
                $transaction->setAmount($amount);
                $transaction->setDescription($description);
                $transaction->setReference($reference);
                $transaction->setDatetime(date('Y-m-d H:i:s'));

                if ($transaction->create()) {
                    Bankaccount::updateAfterTransaction($amount, $idsender, $idreceiver);
                    echo "<div class='container row col-12 line mb-3 m-auto justify-content-center alert alert-success'
                    style='border:1px solid #cecece;'><ul><li>Transaktion erfolgreich! Sie werden auf das Dashboard geleitet!</li></ul></div>";
                    header("Refresh:2; url=dashboard.php");
                    die();
                } else {
                    echo "<div class='container row col-12 line mb-3 m-auto justify-content-center alert alert-danger' 
                    style='border:1px solid #cecece;'><ul><li>Transaktion fehlgeschlagen!</li></ul></div>";
                    header("Refresh:2; url=employee.php");
                    die();
                }

            }
            ?>
            <form action="employee.php" method="post">
                <div class="container border row col-12 mt-5 m-auto border-white rounded-3 bg-dark text-white shadow-lg">
                    <!--    <div class="container row col-12 line m-auto justify-content-center" style="border:1px solid #cecece;">-->

                    <form class="row g-3">
                        <!--Empfänger-->
                        <div class="col-sm-6">
                            <label for="inputReceiver" class="mt-3 form-label">IBAN*</label>
                            <select name="receiver"
                                    class="form-select <?= $user->hasErrors('receiver') ? 'is-invalid' : '' ?>">
                                <option value="" hidden>-- IBAN wählen --</option>
                                <?php
                                foreach ($bankaccounts as $item) {
                                    echo '<option value=' . $item->getIban() . '>' . $item->getIban() . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <!--Betrag-->
                        <div class="col-sm-6">
                            <label for="inputAmount" class="mt-3 form-label">Betrag*</label>
                            <input
                                    type="number"
                                    step="0.01"
                                    name="amount"
                                    class="form-control <?= $user->hasErrors('amount') ? 'is-invalid' : '' ?>"
                                    value="<?= htmlspecialchars($amount) ?>"
                                    id="inputAmount"
                                    maxlength="20"
                                    required
                            />
                        </div>
                        <!--Verwendungszweck-->
                        <div class="col-sm-6">
                            <div class="form-check mt-3">
                                <input class="form-check-input" type="radio" name="bar" value="0" id="auszahlung"
                                       checked>
                                <label class="form-check-label" for="auszahlung">
                                    Barauszahlung
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="bar" value="1" id="einzahlung">
                                <label class="form-check-label" for="einzahlung">
                                    Bareinzahlung
                                </label>
                            </div>
                        </div>
                        <!--Zahlungsreferenz-->
                        <div class="col-sm-6">
                            <label for="inputReference" class="mt-3 form-label">Zahlungsreferenz*</label>
                            <input
                                    type="text"
                                    name="reference"
                                    class="form-control <?= $user->hasErrors('reference') ? 'is-invalid' : '' ?>"
                                    value="<?= htmlspecialchars($reference) ?>"
                                    id="reference"
                                    required
                            />
                        </div>
                        <!--Button-->
                        <div class="col-12 mt-3 mb-3">
                            <input type="submit" name="submit" value="Neu" class="btn btn-primary"></input>
                            <a href="dashboard.php" class="btn btn-secondary ms-3">Zurück</a>
                        </div>
                    </form>
        </main>
    </div>
</div>
</body>
</html>