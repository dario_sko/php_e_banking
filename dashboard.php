<?php
session_start();
require_once "models/User.php";

if (isset($_GET['logout'])) {
    User::logout();
    header("Location: index.php");
    die();
}

if (!User::isLoggedIn()) {
    header("Location: 404.php");
    die();
}

?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <title>E-Banking</title>
</head>
<body>

<header class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
    <a class="navbar-brand col-md-3 col-lg-2 me-0 px-3" href="#">KerberSoki-Bank</a>

    <!--    Logout  -->
    <div class="navbar-nav">
        <div class="nav-item text-nowrap">
            <a class="nav-link px-3" href="?logout=true">Logout</a>
        </div>
    </div>

</header>

<!--  Linke Seite -->
<div class="container-fluid">
    <div class="row">
        <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block">
            <div class="position-sticky pt-3">

                <h5 class="px-3 fs-4">
                    Kundenprofil
                </h5>
                <!--User Profil Ausgabe-->
                <?php
                $user = unserialize($_SESSION['user']);
                ?>

                <ul class="nav flex-column px-3">
                    <li>
                        <p class="my-0 fw-bold">ID:<br></p>
                        <p class="my-0"><?= $user->getIduser() ?></p>
                    </li>
                    <li class="my-0">
                        <p class="my-0 fw-bold">Vorname:<br></p>
                        <p class="my-0"><?= $user->getFirstname() ?></p>
                    </li>
                    <li class="my-0">
                        <p class="my-0 fw-bold">Nachname:<br></p>
                        <p class="my-0"><?= $user->getLastname() ?></p>
                    </li>
                    <li class="my-0">
                        <p class="my-0 fw-bold">Geburtstag:<br></p>
                        <p class="my-0"><?= $user->getBirthdate() ?></p>
                    </li>
                    <li class="my-0">
                        <p class="my-0 fw-bold">E-Mail:<br></p>
                        <p class="my-0"><?= $user->getEmail() ?></p>
                    </li>
                    <li class="my-0">
                        <p class="my-0 fw-bold">Adresse:<br></p>
                        <p class="my-0"><?= $user->getAddress() ?></p>
                    </li>
                    <li class="my-0">
                        <p class="my-0 fw-bold">PLZ:<br></p>
                        <p class="my-0"><?= $user->getPostalcode() ?></p>
                    </li>
                </ul>

                <h5 class="px-3 fs-4 mt-3">
                    Bankkonto
                </h5>
                <!--Bankkonto Ausgabe-->
                <?php
                require_once "models/Bankaccount.php";
                $bankaccount = Bankaccount::get($user->getFkbankaccounts());

                ?>
                <ul class="nav flex-column mb-2 px-3">
                    <li class="my-0">
                        <p class="my-0 fw-bold">Kontostand:<br></p>
                        <p class="my-0"><?= number_format($bankaccount->getBalance(), 2) ?> €</p>
                    </li>
                    <li class="my-0">
                        <p class="my-0 fw-bold">IBAN:<br></p>
                        <p class="my-0"><?= $bankaccount->getIban() ?></p>
                    </li>
                    <li class="my-0">
                        <p class="my-0 fw-bold">BIC:<br></p>
                        <p class="my-0"><?= $bankaccount->getBic() ?></p>
                    </li>
                </ul>
            </div>
        </nav>

        <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4 mt-3">

            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Transaktionen</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <div class="btn-group me-2">
                        <?php
                        if ($user->getIsEmployee() == 1) {
                            echo "<a class='btn btn-primary' href='employee.php'>Mitarbeiter</a>";
                        } else {
                            echo " <a href='transaction.php' class='btn btn-primary'>Neue Transaktion</a>";
                        }
                        ?>

                    </div>
                </div>
            </div>
            <!--Transaktionen Ausgabe-->
            <?php
            require_once "models/Transaction.php";
            if (isset($_POST['submit'])) {

                $textsearch = $_POST['textsearch'] ?? "";
                $amountfrom = $_POST['amountfrom'] ?? "";
                $amountto = $_POST['amountto'] ?? "";
                $datefrom = $_POST['datefrom'] ?? "";
                $dateto = $_POST['dateto'] ?? "";

                $transactions = Transaction::getAllFilteredbyBankaccount($bankaccount->getIdbankaccounts(), $textsearch
                    , $amountfrom, $amountto, $datefrom, $dateto);

            } else if ($user->getIsEmployee() == 1) {
                $transactions = Transaction::getAll();
            } else {
                $transactions = Transaction::getAllByBankacoount($bankaccount->getIdbankaccounts());
            }
            ?>
            <div class="table-responsive">
                <table class="table table-sm">
                    <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Empfänger</th>
                        <th scope="col">Absender</th>
                        <th scope="col">Verwendungszweck</th>
                        <th scope="col">Zahlungsreferenz</th>
                        <th scope="col">Datum/Zeit</th>
                        <th scope="col" colspan="2">Betrag</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($transactions as $transaction) {

                        // Überprüfung für Tabellenzeilefarbe und Vorzeichen
                        if ($transaction->getSenderIbanByTransaction() == $transaction->getReceiverIbanByTransaction()) {
                            $class = "table-info";
                            $sign = "";
                        } else if ($bankaccount->getIban() == $transaction->getSenderIbanByTransaction()) {
                            $class = 'table-danger';
                            $sign = " - ";
                        } else {
                            $class = 'table-success';
                            $sign = "";
                        }

                        // Überprüfung für Ausgabe Empfänger und Absender
                        if ($user->getIsEmployee() == 1) {
                            $receiver = $transaction->getReceiverIbanByTransaction();
                            $sender = $transaction->getSenderIbanByTransaction();
                        } else if ($transaction->getSenderIbanByTransaction() == $transaction->getReceiverIbanByTransaction()) {
                            $receiver = "Eigenübertrag";
                            $sender = "Eigenübertrag";
                        } else if ($bankaccount->getIban() == $transaction->getReceiverIbanByTransaction()) {
                            $receiver = "Kontoeingang";
                            $sender = $transaction->getSenderIbanByTransaction();
                        } else {
                            $receiver = $transaction->getReceiverIbanByTransaction();
                            $sender = "Kontoausgang";
                        }


                        // Ausgabe Tabelle
                        echo "<tr class=" . $class . ">" .
                            "<td>" . $transaction->getIdtransactions() . "</td>
                        <td>" . $receiver . "</td>
                        <td>" . $sender . "</td>
                        <td>" . $transaction->getDescription() . "</td>
                        <td>" . $transaction->getReference() . "</td>
                        <td>" . $transaction->getDateAndTimeFormat() . "</td>
                        <td>" . $sign . "</td>
                        <td class='text-end'>" . number_format($transaction->getAmount(), 2) . " €</td>
                        </tr>";
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            <h5 class="h5">Transaktionssuche</h5>
            <form action="dashboard.php" method="post" class="border-top pt-3 pb-2 mb-3">
                <div>
                    <!--  Text-Suche  -->
                    <div class="row">
                        <div class="col-6">
                            <div class="input-group m-1">
                                <span class="input-group-text">Verwendungszweck</span>
                                <input class="form-control" type="text" placeholder="Transaktionen durchsuchen..."
                                       value="<?= htmlspecialchars($_POST['textsearch'] ?? "") ?>"
                                       name="textsearch">
                            </div>
                        </div>
                    </div>

                    <!--  Betrag-Suche  -->
                    <div class="row">
                        <div class="col-6">
                            <div class="input-group m-1">
                                <span class="input-group-text">Betrag von</span>
                                <input type="number" step="0.01" class="form-control" placeholder="Betrag von"
                                       value="<?= htmlspecialchars($_POST['amountfrom'] ?? "") ?>"
                                       name="amountfrom">
                                <span class="input-group-text">bis</span>
                                <input type="number" step="0.01" class="form-control" placeholder="Betrag bis"
                                       value="<?= htmlspecialchars($_POST['amountto'] ?? "") ?>"
                                       name="amountto">
                            </div>
                        </div>
                    </div>

                    <!--  Datum-Suche  -->
                    <div class="row">
                        <div class="col-6">
                            <div class="input-group m-1">
                                <span class="input-group-text">Datum von</span>
                                <input type="date" class="form-control" name="datefrom"
                                       value="<?= htmlspecialchars($_POST['datefrom'] ?? "") ?>">
                                <span class="input-group-text">bis</span>
                                <input type="date" class="form-control" name="dateto"
                                       value="<?= htmlspecialchars($_POST['dateto'] ?? "") ?>">
                            </div>
                        </div>
                        <div class="col-3">
                            <input type="submit" name="submit" class="btn btn-primary" value="Suche">
                            <a href="dashboard.php" class="btn btn-secondary">Reset</a>
                        </div>
                    </div>
                </div>
            </form>

        </main>
    </div>
</div>


</body>
</html>
