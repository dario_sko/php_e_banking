<?php
session_start();
require_once "models/User.php";
$errorMessage = "";

if (User::isLoggedIn()) {
    header("Location: dashboard.php");
    die();
}

if (isset($_POST['submit'])) {
    $user = new User();

    if ($user->login()) {
        header("Location: dashboard.php");
        die();
    }

    if (!empty($user->getErrors())) {
        $errorMessage = "<div class='alert alert-danger'> <p>Die eingegebenen Daten sind Fehlerhaft</p>";
        $errorMessage .= "<p>" . $user->getErrors()['login'] . "</p></div>";
    }
}

?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <title>E-Banking</title>
</head>
<body>
<!--<h1 class="h1 text-center m-2 text-secondary">KerberSoki-Bank</h1>-->
<header class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow-lg">
    <a class="navbar-brand col-md-3 col-lg-2 me-0 px-3" href="#">KerberSoki-Bank</a>
</header>
<?= $errorMessage ?>
<div class="container-fluid mt-5 col-sm-5 border border-white rounded-3 bg-dark shadow-lg">

    <h2 class="h2 m-3 text-white text-center">Willkommen beim E-Banking</h2>

    <form id="login_form" method="post" action="index.php">
        <div class="col-sm-5 mx-auto m-3 ">
            <input type="text"
                   name="username"
                   required
                   class="form-control
                       <?= isset($errors['login']) ? 'is-invalid' : '' ?>"
                   value="<?= htmlspecialchars("") ?>"
                   placeholder="Benutzername"
                   min="2"
                   max="25"
            />
        </div>

        <div class="col-sm-5 mx-auto m-3">
            <input type="password"
                   name="password"
                   required
                   class="form-control
                       <?= isset($errors['login']) ? 'is-invalid' : '' ?>"
                   value="<?= htmlspecialchars("") ?>"
                   placeholder="Passwort">
        </div>


        <div class="col-sm-5 mx-auto m-3">
            <input type="submit" name="submit" value="Anmelden" class="btn btn-primary w-100"</input>
        </div>
    </form>
    <div class="container m-3">
        <div class="col-sm-3 mx-auto ">
            <input type="button" class="btn btn-secondary" value="Registrieren"
                   onclick="window.location.href='registrierung/registrierung.php'">
        </div>
    </div>
</div>
</div>
</body>
</html>